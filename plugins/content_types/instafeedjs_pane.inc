<?php
/**
 * @file
 * CTools plugin for Instafeed.js implementation.
 */

/**
 * Describe the plugin for CTools.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Instafeed widget pane'),
  'description' => t('Pane with Instagram posts'),
  'category' => t('Widgets'),
  'admin info' => 'instafeedjs_instafeedjs_pane_content_type_admin_info',
  'defaults' => array(
    'instafeedjs_get' => 'popular',
    'instafeedjs_get_value' => '',
    'instafeedjs_limit' => 10,
    'instafeedjs_resolution' => 'thumbnail',
  ),
);

/**
 * Callback for 'admin info' panel pane.
 */
function instafeedjs_instafeedjs_pane_content_type_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = t('Get option: @get.', array('@get' => $conf['instafeedjs_get']));
    $block->content .= !empty($conf['instafeedjs_get_value']) ? '<br />' . t('Get value: @get_val.', array('@get_val' => $conf['instafeedjs_get_value'])) : '';
    $block->content .= '<br />' . t('Limit: @limit.', array('@limit' => $conf['instafeedjs_limit']));
    $block->content .= '<br />' . t('Resolution: @resolution.', array('@resolution' => $conf['instafeedjs_resolution']));
    return $block;
  }
}

/**
 * Ctools edit form callback.
 */
function instafeedjs_instafeedjs_pane_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];

  $defaults = array(
    'instafeedjs_get' => $conf['instafeedjs_get'],
    'instafeedjs_get_value' => $conf['instafeedjs_get_value'],
    'instafeedjs_limit' => $conf['instafeedjs_limit'],
    'instafeedjs_resolution' => $conf['instafeedjs_resolution'],
  );
  $form += instafeedjs_configuration_form($defaults);

  return $form;
}

/**
 * Ctools validation form callback.
 */
function instafeedjs_instafeedjs_pane_content_type_edit_form_validate(&$form, &$form_state) {
  $get = $form_state['values']['instafeedjs_get'];
  $get_val = $form_state['values']['instafeedjs_get_value'];
  if (in_array($get, array('tagged', 'location')) && empty($get_val)) {
    form_set_error('instafeedjs_get_value', t('You should specify get value.'));
  }
}

/**
 * Ctools submit form callback.
 */
function instafeedjs_instafeedjs_pane_content_type_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['instafeedjs_get'] = $form_state['values']['instafeedjs_get'];
  $form_state['conf']['instafeedjs_get_value'] = $form_state['values']['instafeedjs_get_value'];
  $form_state['conf']['instafeedjs_limit'] = $form_state['values']['instafeedjs_limit'];
  $form_state['conf']['instafeedjs_resolution'] = $form_state['values']['instafeedjs_resolution'];
}

/**
 * Ctools render callback.
 */
function instafeedjs_instafeedjs_pane_content_type_render($subtype, $conf, $panel_args, $context) {
  global $language;
  $config = array(
    'instafeedjs_get' => $conf['instafeedjs_get'],
    'instafeedjs_get_value' => $conf['instafeedjs_get_value'],
    'instafeedjs_limit' => $conf['instafeedjs_limit'],
    'instafeedjs_resolution' => $conf['instafeedjs_resolution'],
  );
  $block = new stdClass();
  $block->title = t('Title of instafeedjs block');
  $block->content = instafeedjs_js($config, $language->language);
  return $block;
}
