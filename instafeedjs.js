/**
 * @file
 *
 * Configure and run Instafeed instance.
 */
(function () {
  "use strict";

  Drupal.behaviors.instafeedsj = function (context) {
    var instafeed_instances = [];
    var settings = Drupal.settings.instafeedjs;
    for (var i in settings) {
      instafeed_instances[settings[i].instance] = new Instafeed(settings[i]);
      instafeed_instances[settings[i].instance].run();
    }
  };

})();
