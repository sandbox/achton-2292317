INTRODUCTION
------------
The Instafeedjs module adds a simple configurable block that renders an
Instagram feed or stream using the instafeed.js JavaScript library.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/instafeedjs
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/instafeedjs

REQUIREMENTS
------------
This module requires the following modules + library:
 * CTools (https://www.drupal.org/project/ctools)
 * Libraries API (https://www.drupal.org/project/libraries)
 * Instafeed.js (http://instafeedjs.com/)

RECOMMENDED MODULES
-------------------
 * Panels (https://www.drupal.org/project/panels):
   When enabled, InstafeedJS will enable the placement of Panels panes which
   render Instagram posts, similar to what is possible with core Blocks.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-5-6
   for further information.
 * Download the Instafeed.js library (instafeed.min.js) and place it in a
   "instafeed" folder in your libraries path, eg.
   sites/all/libraries/instafeed/instafeed.min.js

CONFIGURATION
-------------
 * Go to /admin/settings/instafeedjs to configure the module after installation.
 * You will need a valid client id from Instagram's API. You can easily register
   for one at http://instagram.com/developer/register/
 * Note that configuration is separated by language, ie. each site language has
   its own config scope.

MAINTAINERS
-----------
Current maintainers:
 * Angela Raetchi (ange_l_ina) - https://www.drupal.org/user/2818973
 * Achton Smidt Winther (achton) - https://drupal.org/user/712454

This project has been sponsored by:
 * Peytz & Co (http://peytz.com)
 * University Post (http://universitypost.dk/university-post)
